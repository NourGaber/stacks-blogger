<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log as Log;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function LogProjects(Request $request){
        $data = $request->all();
        $orderLog = new Logger('order');
        $orderLog->pushHandler(new StreamHandler(storage_path().'/logs/'.$data['project'].'.log'), Logger::INFO);
        $orderLog->info($data['message']);
        Log::channel('papertrail')->info($data['project'].' '.$data['message']);
    }
}

